package com.example.demo.config;

import org.springframework.security.core.userdetails.UserDetails;

public interface PlatformUser extends UserDetails {
}