package com.example.demo.core.exception;

public class ResourceNotFoundException extends RuntimeException {

    public ResourceNotFoundException(Class<?> clazz, final Long id) {
        super(String.format("Resource [%s] with identifier [%s] does not exist", clazz.getSimpleName(), id));
    }

    public ResourceNotFoundException(Class<?> clazz, final String name) {
        super(String.format("Resource [%s] with identifier [%s] does not exist", clazz.getSimpleName(), name));
    }
}