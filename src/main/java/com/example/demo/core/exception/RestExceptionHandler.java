package com.example.demo.core.exception;

import com.example.demo.core.domain.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(ResourceNotFoundException.class)
    public final ResponseEntity<Object> handleResourceNotFoundException(final ResourceNotFoundException ex) {
        final List<Object> errors = new ArrayList<>();
        errors.add(ex.getMessage());
        final var error = new ApiError("Record Not Found", errors);
        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }
}