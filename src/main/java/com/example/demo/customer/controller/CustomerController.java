package com.example.demo.customer.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/customer")
public class CustomerController {

    @Value("${spring.application.name}")
    String appName;

    @GetMapping
    public String customerPage(Model model) {
        model.addAttribute("appName", appName);
        return "customer";
    }

    @GetMapping("/v1")
    public String customerPageV1(Model model) {
        model.addAttribute("appName", appName);
        return "customer";
    }
}
