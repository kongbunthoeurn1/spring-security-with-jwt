package com.example.demo.customer.controller;

import com.example.demo.customer.domain.Customer;
import com.example.demo.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/customer")
public class CustomerRestController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/{id}")
    public Customer get(@PathVariable final Long id) {
        return customerService.findById(id);
    }

    @GetMapping("/name/{name}")
    public Customer get(@PathVariable final String name) {
        return customerService.findByName(name);
    }

    @GetMapping
    public List<Customer> listCustomer() {
        return customerService.findAll();
    }

    @PostMapping
    public Customer create(@RequestBody Customer customer) {
        return customerService.save(customer);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        customerService.delete(id);
    }

    @PutMapping("/{id}")
    public Customer updateBook(@RequestBody Customer customer,
                               @PathVariable Long id) {

        return customerService.update(id, customer);
    }
}