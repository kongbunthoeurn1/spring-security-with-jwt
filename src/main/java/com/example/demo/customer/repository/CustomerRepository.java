package com.example.demo.customer.repository;

import com.example.demo.customer.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    // rawSQL = select * from customer where name = $name
    Optional<Customer> findByName(final String name);
}
