package com.example.demo.customer.service;

import com.example.demo.customer.domain.Customer;

import java.util.List;

public interface CustomerService {

    Customer findById(final Long id);

    Customer save(final Customer requestBody);

    List<Customer> findAll();

    void delete(final Long id);

    Customer update(final Long id, final Customer customer);

    Customer findByName(final String name);
}
