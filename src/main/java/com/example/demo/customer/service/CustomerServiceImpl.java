package com.example.demo.customer.service;

import com.example.demo.core.exception.ResourceNotFoundException;
import com.example.demo.customer.domain.Customer;
import com.example.demo.customer.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CustomerServiceImpl implements CustomerService{

    private final CustomerRepository repository;

    @Override
    public Customer findById(final Long id) {
        // find data by id if null throw exception
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(Customer.class, id));
    }

    @Override
    @Transactional
    public Customer save(final Customer requestBody) {
        return repository.save(requestBody);
    }

    @Override
    public List<Customer> findAll() {
        return repository.findAll();
    }

    @Override
    @Transactional
    public void delete(final Long id) {
        final var customer = findById(id);
        repository.delete(customer);
    }

    @Override
    @Transactional
    public Customer update(final Long id, final Customer updateCustomer) {
        // validate customer exist or not
        findById(id);

        // update
        updateCustomer.setId(id);
        return repository.save(updateCustomer);
    }

    @Override
    public Customer findByName(final String name) {
        return repository.findByName(name).orElseThrow(() -> new ResourceNotFoundException(Customer.class, name));
    }
}