package com.example.demo.user.domain;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AppUserData {

    private String username;
    private String password;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
}
