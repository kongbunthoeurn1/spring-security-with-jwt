package com.example.demo.user.service;

import com.example.demo.user.domain.AppUser;
import com.example.demo.user.domain.AppUserData;
import com.example.demo.user.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AppUserService {

    private final PasswordEncoder bcryptEncoder;
    private final AppUserRepository appUserRepository;

    public AppUser save(AppUserData user) {
        var newUser = new AppUser();
        newUser.setUsername(user.getUsername());
        newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
        newUser.setEnabled(true);
        newUser.setAccountNonExpired(true);
        newUser.setAccountNonLocked(true);
        newUser.setCredentialsNonExpired(true);
        return appUserRepository.save(newUser);
    }
}
